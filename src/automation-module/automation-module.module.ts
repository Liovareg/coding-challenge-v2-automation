import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutomationPanelComponent } from './automation-panel/automation-panel.component';
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [AutomationPanelComponent],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [AutomationPanelComponent],
})
export class AutomationModuleModule {}
