import { Component, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { predictElements } from "../automation.util";
import { AutomationStep } from "../automation-step.constant";
import { FormControl } from "@angular/forms";

@Component({
  selector: 'automation-panel',
  templateUrl: './automation-panel.component.html',
  styleUrls: ['./automation-panel.component.css'],
})
export class AutomationPanelComponent {
  constructor(private renderer: Renderer2, private elRef: ElementRef) {
  }

  readonly AutomationStep = AutomationStep;

  populateTextControl = new FormControl('');
  currentStep: AutomationStep = AutomationStep.SELECT_ELEMENT;

  get elementSelectionAllowed(): boolean {
    return this.currentStep === AutomationStep.SELECT_ELEMENT;
  }

  get selectedElementsNumber(): number {
    return this.selectedElements.size;
  }

  get predictedElementsNumber(): number {
    return this.predictedElements.size;
  }

  get selectedAndPredictedElements(): HTMLElement[] {
    return [...this.selectedElements.values(), ...this.predictedElements.values()]
  }

  get resetButtonAllowed(): boolean {
    return this.currentStep !== AutomationStep.SELECT_ELEMENT &&
      this.currentStep !== AutomationStep.ALL_DONE;
  }

  private selectedElements: Set<HTMLElement> = new Set<HTMLElement>();
  private predictedElements: Set<HTMLElement> = new Set<HTMLElement>();

  @HostListener('document:mouseover', ['$event'])
  onMouseOver(e: any): void {
    const el: HTMLElement = e.target;

    if (this.isAutomationElement(el) || !this.elementSelectionAllowed) {
      return;
    }

    this.renderer.addClass(el, 'highlighted');

    el.addEventListener('click', this.clickInterceptor, true);
  }

  @HostListener('document:mouseout', ['$event'])
  onMouseOut(e: any): void {
    const el: HTMLElement = e.target;

    if (this.isAutomationElement(el) || !this.elementSelectionAllowed) {
      return;
    }

    this.renderer.removeClass(el, 'highlighted');
    el.removeEventListener('click', this.clickInterceptor, true);
  }

  private toggleClickedElement(el: HTMLElement): void {
    if (this.selectedElements.delete(el)) {
      this.renderer.removeClass(el, 'selected');
    } else {
      this.selectedElements.add(el);
      this.renderer.addClass(el, 'selected');
    }
  }

  private clickInterceptor = (e: MouseEvent) => {
    e.stopImmediatePropagation();

    this.toggleClickedElement(e.target as HTMLElement);

    const predictedElements = predictElements(this.selectedElements);
    this.predictedElements.forEach(el => this.renderer.removeClass(el, 'predicted'));
    this.predictedElements.clear();
    predictedElements.forEach(el => this.predictedElements.add(el));
    this.predictedElements.forEach(el => this.renderer.addClass(el, 'predicted'));
  };

  private isAutomationElement(el: HTMLElement) {
    return this.elRef.nativeElement.contains(el);
  }

  /* Click handlers */

  handleApproveSelectionClicked(): void {
    this.currentStep = AutomationStep.SELECT_ACTION
  }

  handleResetClicked(): void {
    this.selectedElements.forEach(el => this.renderer.removeClass(el, 'selected'));
    this.predictedElements.forEach(el => this.renderer.removeClass(el, 'predicted'));
    this.selectedElements.clear();
    this.predictedElements.clear();

    this.currentStep = AutomationStep.SELECT_ELEMENT;
  }

  handleClickElementsClicked(): void {
    this.selectedAndPredictedElements.forEach(el => el.click());

    this.currentStep = AutomationStep.ALL_DONE;
  }

  handlePopulateInputActionClicked(): void {
    this.currentStep = AutomationStep.SELECT_INPUT;
  }

  handlePerformPopulateInputClicked(): void {
    const value = this.populateTextControl.value ?? '';
    this.selectedAndPredictedElements.forEach(el => {
      this.renderer.setAttribute(el, 'value', value)
    });

    this.currentStep = AutomationStep.ALL_DONE;
  }

  handleSelectActionClicked(): void {
    this.currentStep = AutomationStep.SELECT_ACTION;
  }
}
